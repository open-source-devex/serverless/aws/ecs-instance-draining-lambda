package com.gitlab.osdevex.ecs.instancedraining.aws;

import com.amazonaws.services.autoscaling.AmazonAutoScaling;
import com.amazonaws.services.autoscaling.AmazonAutoScalingClientBuilder;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import com.amazonaws.services.ecs.AmazonECS;
import com.amazonaws.services.ecs.AmazonECSClientBuilder;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.AmazonSNSClientBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AwsConfiguration {

    @Bean
    public AwsService awsService() {
        return new AwsService(new Ec2Facade(ec2Client()), new EcsFacade(ecsClient()), new AutoScalingFacade(autoScalingClient()), new SnsFacade(snsClient()));
    }

    private AmazonEC2 ec2Client() {
        return AmazonEC2ClientBuilder.defaultClient();
    }

    private AmazonECS ecsClient() {
        return AmazonECSClientBuilder.defaultClient();
    }

    private AmazonAutoScaling autoScalingClient() {
        return AmazonAutoScalingClientBuilder.defaultClient();
    }

    private AmazonSNS snsClient() {
        return AmazonSNSClientBuilder.defaultClient();
    }
}

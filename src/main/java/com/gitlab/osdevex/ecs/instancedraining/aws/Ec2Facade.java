package com.gitlab.osdevex.ecs.instancedraining.aws;

import javax.xml.bind.DatatypeConverter;

import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.model.DescribeInstanceAttributeRequest;
import com.amazonaws.services.ec2.model.DescribeInstanceAttributeResult;
import com.amazonaws.services.ec2.model.InstanceAttribute;
import org.jetbrains.annotations.NotNull;

class Ec2Facade {

    static final String USER_DATA_ATTRIBUTE = "userData";

    @NotNull
    private final AmazonEC2 ec2Client;

    Ec2Facade(final AmazonEC2 ec2Client) {
        this.ec2Client = ec2Client;
    }

    @NotNull
    String getInstanceUserData(final @NotNull String ec2InstanceId) {
        final InstanceAttribute instanceAttribute = getInstanceAttribute(ec2InstanceId, USER_DATA_ATTRIBUTE);
        final String base64EncodedUserData = instanceAttribute.getUserData();
        return decodeBase64(base64EncodedUserData);
    }

    @NotNull
    private String decodeBase64(final @NotNull String base64EncodedUserData) {
        return new String(DatatypeConverter.parseBase64Binary(base64EncodedUserData));
    }

    private InstanceAttribute getInstanceAttribute(final @NotNull String ec2InstanceId, final @NotNull String attribute) {
        final DescribeInstanceAttributeResult encodedUserDataResult = ec2Client.describeInstanceAttribute(new DescribeInstanceAttributeRequest(ec2InstanceId, attribute));
        return encodedUserDataResult.getInstanceAttribute();
    }
}

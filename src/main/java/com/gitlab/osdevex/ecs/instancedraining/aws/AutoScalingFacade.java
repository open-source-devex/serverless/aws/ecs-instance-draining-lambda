package com.gitlab.osdevex.ecs.instancedraining.aws;

import com.amazonaws.services.autoscaling.AmazonAutoScaling;
import com.amazonaws.services.autoscaling.model.CompleteLifecycleActionRequest;
import org.jetbrains.annotations.NotNull;

class AutoScalingFacade {

    private static final String ABANDON = "ABANDON";
    private static final String CONTINUE = "CONTINUE";

    private final AmazonAutoScaling amazonAutoScaling;

    AutoScalingFacade(final AmazonAutoScaling amazonAutoScaling) {
        this.amazonAutoScaling = amazonAutoScaling;
    }

    void abortLifecycle(final @NotNull String lifecycleHookName, final @NotNull String autoScalingGroupName, final @NotNull String instanceId) {
        final CompleteLifecycleActionRequest request = buildCompleteLifecycleActionRequest(lifecycleHookName, autoScalingGroupName, instanceId, ABANDON);

        amazonAutoScaling.completeLifecycleAction(request);
    }

    void continueLifecycle(final @NotNull String lifecycleHookName, final @NotNull String autoScalingGroupName, final @NotNull String instanceId) {
        final CompleteLifecycleActionRequest request = buildCompleteLifecycleActionRequest(lifecycleHookName, autoScalingGroupName, instanceId, CONTINUE);

        amazonAutoScaling.completeLifecycleAction(request);
    }

    @NotNull
    private CompleteLifecycleActionRequest buildCompleteLifecycleActionRequest(final @NotNull String lifecycleHookName, final @NotNull String autoScalingGroupName,
                                                                               final @NotNull String instanceId, final @NotNull String result) {
        final CompleteLifecycleActionRequest request = new CompleteLifecycleActionRequest();
        request.setAutoScalingGroupName(autoScalingGroupName);
        request.setLifecycleHookName(lifecycleHookName);
        request.setInstanceId(instanceId);
        request.setLifecycleActionResult(result);
        return request;
    }
}

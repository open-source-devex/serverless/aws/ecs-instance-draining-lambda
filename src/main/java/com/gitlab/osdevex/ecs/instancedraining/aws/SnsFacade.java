package com.gitlab.osdevex.ecs.instancedraining.aws;

import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.model.PublishResult;
import org.jetbrains.annotations.NotNull;

public class SnsFacade {

    private final AmazonSNS amazonSNS;

    public SnsFacade(final @NotNull AmazonSNS amazonSNS) {
        this.amazonSNS = amazonSNS;
    }

    public String pushEvent(final String topicArn, final String message, final String subject) {
        final PublishResult result = amazonSNS.publish(topicArn, message, subject);
        return result.getMessageId();
    }
}

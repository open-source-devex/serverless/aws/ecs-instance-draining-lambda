package com.gitlab.osdevex.ecs.instancedraining.aws;

import com.amazonaws.services.ecs.model.ContainerInstance;
import org.jetbrains.annotations.NotNull;

public class AwsService {

    private final Ec2Facade ec2Facade;
    private final EcsFacade ecsFacade;
    private final AutoScalingFacade autoScalingFacade;
    private final SnsFacade snsFacade;

    public AwsService(final Ec2Facade ec2Facade, final EcsFacade ecsFacade, final AutoScalingFacade autoScalingFacade, final SnsFacade snsFacade) {
        this.ec2Facade = ec2Facade;
        this.ecsFacade = ecsFacade;
        this.autoScalingFacade = autoScalingFacade;
        this.snsFacade = snsFacade;
    }

    public String findEcsClusterName(final @NotNull String ec2InstanceId) throws EcsClusterNameNotFoundException {
        final String userData = ec2Facade.getInstanceUserData(ec2InstanceId);
        return Ec2InstanceUserDataScraper.scrapeEcsCluster(userData)
                                         .orElseThrow(() -> new EcsClusterNameNotFoundException("Could not find ECS cluster name in user data of instance: " + ec2InstanceId));
    }

    public ContainerInstance containerInstance(final @NotNull String ecsCluster, final @NotNull String ec2InstanceId) throws InstanceNotFoundException {
        return ecsFacade.containerInstance(ecsCluster, ec2InstanceId);
    }

    public void abortLifecycle(final @NotNull String lifecycleHookName, final @NotNull String autoScalingGroupName, final @NotNull String instanceId) {
        autoScalingFacade.abortLifecycle(lifecycleHookName, autoScalingGroupName, instanceId);
    }

    public int countTasksRunningOnInstance(final @NotNull String clusterName, final @NotNull String containerInstanceId) {
        return ecsFacade.countTasksRunningOnInstance(clusterName, containerInstanceId);
    }

    public void continueLifecycle(final @NotNull String lifecycleHookName, final @NotNull String autoScalingGroupName, final @NotNull String instanceId) {
        autoScalingFacade.continueLifecycle(lifecycleHookName, autoScalingGroupName, instanceId);
    }

    public void drainInstance(final String clusterName, final ContainerInstance containerInstance) throws DrainInstanceException {
        ecsFacade.drainInstance(clusterName, containerInstance);
    }

    public String pushSnsNotification(final String message, final String topicArn, final String subject) {
        return snsFacade.pushEvent(message, topicArn, subject);
    }
}

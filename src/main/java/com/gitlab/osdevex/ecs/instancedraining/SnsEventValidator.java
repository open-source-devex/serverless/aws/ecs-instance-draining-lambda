package com.gitlab.osdevex.ecs.instancedraining;

import java.io.IOException;
import java.util.Objects;

import com.amazonaws.services.lambda.runtime.events.SNSEvent;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;

class SnsEventValidator {

    static final String EVENT_MUST_HAVE_RECORDS = "Event must have records";
    static final String FIRST_EVENT_RECORD_MUST_HAVE_A_MESSAGE = "First event record must have a message";
    static final String FIRST_EVENT_RECORD_MESSAGE_WAS_NOT_OF_CORRECT_TYPE = "First event record message was not of type SnsEventLifecycleHookMessage";
    static final String NOTIFICATION_METADATA_WAS_NOT_OF_CORRECT_TYPE = "Notification metadata was not of type SnsNotificationMetaData";
    static final String IO_EXCEPTION_CAUGHT_UNMARSHALLING_IN_MEMORY_STRING = "IOException caught unmarshalling in-memory string";
    private static final String LIFECYCLE_TRANSITION_INSTANCE_TERMINATE = "autoscaling:EC2_INSTANCE_TERMINATING";

    private final ObjectMapper mapper;

    SnsEventValidator(final ObjectMapper mapper) {
        this.mapper = mapper;
    }

    void validateEvent(@NotNull final SNSEvent event, @NotNull final String lifecycleHookName) {
        validateRecords(event);

        final String message = validateMessage(event);

        final SnsEventLifecycleHookMessage lifecycleHookMessage = validateLifecycleHookMessage(message);
        validateNotificationMetadata(lifecycleHookMessage);
        Objects.requireNonNull(lifecycleHookMessage.getEc2InstanceId());
        Objects.requireNonNull(lifecycleHookMessage.getAutoScalingGroupName());

        validateLifecycleHookName(lifecycleHookMessage);
        validateLifecycleTransition(lifecycleHookMessage);
    }

    private void validateRecords(final @NotNull SNSEvent event) {
        if (event.getRecords().isEmpty()) {
            throw new IllegalArgumentException(EVENT_MUST_HAVE_RECORDS);
        }
    }

    @NotNull
    private String validateMessage(final @NotNull SNSEvent event) {
        final String message = event.getRecords().get(0).getSNS().getMessage();
        if (message == null || message.isEmpty()) {
            throw new IllegalArgumentException(FIRST_EVENT_RECORD_MUST_HAVE_A_MESSAGE);
        }
        return message;
    }

    private void validateLifecycleTransition(final SnsEventLifecycleHookMessage lifecycleHookMessage) {
        if (!LIFECYCLE_TRANSITION_INSTANCE_TERMINATE.equals(lifecycleHookMessage.getLifecycleTransition())) {
            throw new IllegalArgumentException("Lifecycle transition should be " + LIFECYCLE_TRANSITION_INSTANCE_TERMINATE);
        }
    }

    private void validateLifecycleHookName(final SnsEventLifecycleHookMessage lifecycleHookMessage) {
        final String lifecycleHookName = lifecycleHookMessage.getLifecycleHookName();
        if (!lifecycleHookName.equals(lifecycleHookName)) {
            throw new IllegalArgumentException("Wrong lifecycle hook name: got " + lifecycleHookName + " expected " + lifecycleHookName);
        }
        Objects.requireNonNull(lifecycleHookName);
    }

    private SnsEventLifecycleHookMessage validateLifecycleHookMessage(final String message) {
        final SnsEventLifecycleHookMessage lifecycleHookMessage;
        try {
            lifecycleHookMessage = mapper.readValue(message, SnsEventLifecycleHookMessage.class);
        } catch (JsonParseException | JsonMappingException e) {
            throw new IllegalArgumentException(FIRST_EVENT_RECORD_MESSAGE_WAS_NOT_OF_CORRECT_TYPE, e);
        } catch (final IOException e) {
            throw new RuntimeException(IO_EXCEPTION_CAUGHT_UNMARSHALLING_IN_MEMORY_STRING, e);
        }
        return lifecycleHookMessage;
    }

    private void validateNotificationMetadata(final SnsEventLifecycleHookMessage lifecycleHookMessage) {
        final String notificationMetadata = lifecycleHookMessage.getNotificationMetadata();
        Objects.requireNonNull(notificationMetadata);
        try {
            mapper.readValue(notificationMetadata, SnsNotificationMetadata.class);
        } catch (JsonParseException | JsonMappingException e) {
            throw new IllegalArgumentException(NOTIFICATION_METADATA_WAS_NOT_OF_CORRECT_TYPE, e);
        } catch (final IOException e) {
            throw new RuntimeException(IO_EXCEPTION_CAUGHT_UNMARSHALLING_IN_MEMORY_STRING, e);
        }
    }
}

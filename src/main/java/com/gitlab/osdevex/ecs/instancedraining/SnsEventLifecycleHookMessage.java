package com.gitlab.osdevex.ecs.instancedraining;

import com.amazonaws.services.autoscaling.model.LifecycleHook;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@SuppressWarnings("unused")
@JsonIgnoreProperties(ignoreUnknown = true)
class SnsEventLifecycleHookMessage extends LifecycleHook {
    private String ec2InstanceId;

    public String getEc2InstanceId() {
        return ec2InstanceId;
    }
}

package com.gitlab.osdevex.ecs.instancedraining.aws;

import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.vavr.collection.Stream;
import io.vavr.control.Option;
import org.jetbrains.annotations.NotNull;

class Ec2InstanceUserDataScraper {

    private static final String ECS_CLUSTER_VARIABLE_ASSIGNMENT = ".*ECS_CLUSTER=([\"'])?([\\w\\d._-]+).*(\n)?";

    static Optional<String> scrapeEcsCluster(@NotNull final String userData) {
        return new Ec2InstanceUserDataScraper().ecsCluster(userData).toJavaOptional();
    }

    @NotNull
    Option<String> ecsCluster(final @NotNull String userData) {
        return Stream.of(splitLines(userData))
                     .filter(isEcsClusterVariableAssignment())
                     .map(getVariableAssignmentValue())
                     .headOption();
    }

    @NotNull
    private Function<@NotNull String, @NotNull String> getVariableAssignmentValue() {
        return this::extractEcsClusterValue;
    }

    @NotNull
    String extractEcsClusterValue(final @NotNull String variableAssignment) {
        final Pattern pattern = Pattern.compile(ECS_CLUSTER_VARIABLE_ASSIGNMENT);
        final Matcher matcher = pattern.matcher(variableAssignment);
        return matcher.matches() ? matcher.group(2) : "";
    }

    @NotNull
    private Predicate<@NotNull String> isEcsClusterVariableAssignment() {
        return line -> line.matches(ECS_CLUSTER_VARIABLE_ASSIGNMENT);
    }

    @NotNull
    private String[] splitLines(final @NotNull String userData) {
        return userData.split("\n");
    }
}

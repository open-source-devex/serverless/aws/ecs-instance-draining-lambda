package com.gitlab.osdevex.ecs.instancedraining.aws;

import java.util.List;

import com.amazonaws.services.ecs.model.Failure;

public class DrainInstanceException extends Exception {

    private final List<Failure> failures;

    DrainInstanceException(final List<Failure> failures) {
        this.failures = failures;
    }

    public List<Failure> getFailures() {
        return failures;
    }
}

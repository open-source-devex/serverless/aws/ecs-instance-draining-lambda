package com.gitlab.osdevex.ecs.instancedraining;

public class SnsNotificationMetadata {
    private int ecsInstanceDaemonTasksCount = 0;

    public int getEcsInstanceDaemonTasksCount() {
        return ecsInstanceDaemonTasksCount;
    }

    public void setEcsInstanceDaemonTasksCount(final int ecsInstanceDaemonTasksCount) {
        this.ecsInstanceDaemonTasksCount = ecsInstanceDaemonTasksCount;
    }
}

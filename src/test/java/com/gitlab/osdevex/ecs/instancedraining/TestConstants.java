package com.gitlab.osdevex.ecs.instancedraining;

public class TestConstants {

    public static String EVENT = "";
    public static String CLUSTER_NAME = "myCluster3_0-1";
    public static String EC2_INSTANCE_ID = "i-049ecacba91f03065";
    public static String USER_DATA = "";
    public static final String LIFECYCLE_HOOK_NAME = "insatnce-draining";

    static {
        EVENT += "{\n";
        EVENT += "  \"records\": [\n";
        EVENT += "    {\n";
        EVENT += "      \"sns\": {\n";
        EVENT += "        \"messageAttributes\": {},\n";
        EVENT += "        \"signingCertUrl\": \"https://sns.eu-central-1.amazonaws.com/SimpleNotificationService-433026a4050d206028891664da859041.pem\",\n";
        EVENT += "        \"messageId\": \"8958c0ec-e3d5-54ca-b772-924fff02466e\",\n";
        EVENT += "        \"message\": \"{\\\"LifecycleHookName\\\":\\\"" + LIFECYCLE_HOOK_NAME + "\\\",\\\"AccountId\\\":\\\"415033069301\\\",";
        EVENT += "\\\"RequestId\\\":\\\"11150dbf-6b9c-45a1-a4d0-f149a23c68e4\\\",\\\"LifecycleTransition\\\":\\\"autoscaling:EC2_INSTANCE_TERMINATING\\\",";
        EVENT += "\\\"AutoScalingGroupName\\\":\\\"cfStackAsgDevContainerInstance-asgDevContainerInstance-ETXVT9T19H07\\\",\\\"Service\\\":\\\"AWS Auto Scaling\\\",";
        EVENT += "\\\"Time\\\":\\\"2017-12-07T10:38:22.081Z\\\",\\\"EC2InstanceId\\\":\\\"" + EC2_INSTANCE_ID + "\\\",\\\"NotificationMetadata\\\":\\\"{\\\\n  ";
        EVENT += "\\\\\\\"ecsInstanceDaemonTasksCount\\\\\\\": \\\\\\\"1\\\\\\\"\\\\n}\\\\n\\\",\\\"LifecycleActionToken\\\":\\\"b12dbc8a-28ed-4820-993d-1723e5b87783\\\"}\",\n";
        EVENT += "        \"subject\": \"Pushing back SNS message to re-trigger lambda\",\n";
        EVENT += "        \"unsubscribeUrl\": \"https://sns.eu-central-1.amazonaws";
        EVENT += ".com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:eu-central-1:415033069301:nloGateway-dev-autoscaling-topic:4d8d2583-d67e-494a-a1f8-bc7b9fd8f0a6\",\n";
        EVENT += "        \"type\": \"Notification\",\n";
        EVENT += "        \"signatureVersion\": \"1\",\n";
        EVENT += "        \"signature\": \"CAVMyfmgdziDUv1CS25fz9yWinpsCE9bTsg4v+XeQSUdZbFhgiHzuuXn8DoPq2r0TqCJUQ/s87s5VdDdZQ2pR2hv4mj/lz9aA8nMX0NbLCz5F9GhGJCkL9";
        EVENT += "+GYbFoWw19VMGSKFPP3WXwOlM5GyO72OY8e3pyf0YKha+J7c2sTs8SwFS0Oqqne/5v/e9HWSc+er2WvUOSyc6kT9Iuh5+JAxR0nyKE4OYQCVwG88Rbv+5FHgpe1gfjS8gHP/iII1NVWnwTY";
        EVENT += "+v7iPh0G579c35Zu7fqaVhDRZJaM3jTIRBclZK6il61t2fc9wjlNPrHWY7Kg+88EQguUMcJ1jenxWtcog==\",\n";
        EVENT += "        \"timestamp\": 1512643223982,\n";
        EVENT += "        \"topicArn\": \"arn:aws:sns:eu-central-1:415033069301:nloGateway-dev-autoscaling-topic\"\n";
        EVENT += "      },\n";
        EVENT += "      \"eventVersion\": \"1.0\",\n";
        EVENT += "      \"eventSource\": \"aws:sns\",\n";
        EVENT += "      \"eventSubscriptionArn\": \"arn:aws:sns:eu-central-1:415033069301:nloGateway-dev-autoscaling-topic:4d8d2583-d67e-494a-a1f8-bc7b9fd8f0a6\"\n";
        EVENT += "    }\n";
        EVENT += "  ]\n";
        EVENT += "}";
    }

    static {
        USER_DATA += "Mime-Version: 1.0\n";
        USER_DATA += "\n";
        USER_DATA += "";
        USER_DATA += "#cloud-config\n";
        USER_DATA += "";
        USER_DATA += "\n";
        USER_DATA += "bootcmd:\n";
        USER_DATA += "  - mkdir -p /etc/ecs\n";
        USER_DATA += "  - echo 'ECS_CLUSTER=" + CLUSTER_NAME + "' >> /etc/ecs/ecs.config\n";
        USER_DATA += "\n";
    }
}

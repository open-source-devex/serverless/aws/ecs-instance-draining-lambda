package com.gitlab.osdevex.ecs.instancedraining.aws;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.model.PublishResult;
import org.junit.jupiter.api.Test;

class SnsFacadeTest {

    public static final String TOPIC_ARN = "topicArn";
    public static final String MESSAGE = "message";
    public static final String SUBJECT = "subject";
    public static final String MESSAGE_ID = "messageId";
    private final AmazonSNS amazonSNSClient = mock(AmazonSNS.class);

    @Test
    void publish() throws Exception {
        final PublishResult result = new PublishResult();
        result.setMessageId(MESSAGE_ID);
        when(amazonSNSClient.publish(TOPIC_ARN, MESSAGE, SUBJECT)).thenReturn(result);
        final String messageId = new SnsFacade(amazonSNSClient).pushEvent(TOPIC_ARN, MESSAGE, SUBJECT);

        assertThat(messageId).isNotEmpty()
                             .isEqualTo(MESSAGE_ID);
    }
}

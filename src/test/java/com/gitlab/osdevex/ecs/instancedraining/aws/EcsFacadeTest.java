package com.gitlab.osdevex.ecs.instancedraining.aws;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import com.amazonaws.services.ecs.AmazonECS;
import com.amazonaws.services.ecs.model.ContainerInstance;
import com.amazonaws.services.ecs.model.ContainerInstanceStatus;
import com.amazonaws.services.ecs.model.DescribeContainerInstancesRequest;
import com.amazonaws.services.ecs.model.DescribeContainerInstancesResult;
import com.amazonaws.services.ecs.model.InvalidParameterException;
import com.amazonaws.services.ecs.model.ListContainerInstancesRequest;
import com.amazonaws.services.ecs.model.ListContainerInstancesResult;
import org.junit.jupiter.api.Test;

class EcsFacadeTest {

    private static final String CLUSTER = "cluster";
    private static final String INSTANCE_ID = "instanceId";
    private static final String CONTAINER_INSTANCE_EMPTY_ERROR = "Container instance cannot be empty. (Service: AmazonECS; Status Code: 400;";
    private static final String INSTANCE_ARN = "instanceArn";
    private static final String SOME_ARN = "someArn";
    private static final String OTHER_ARN = "otherArn";
    private static final String SOME_INSTANCE_ID = "someInstanceId";

    public static final String OTHER_INSTANCE_ID = "otherInstanceId";

    private final AmazonECS ecsClient = mock(AmazonECS.class);

    @Test
    void containerInstanceStatus_whenInstanceCannotBeFoundBecauseClusterHasNoInstances() throws Exception {
        // List instances in cluster returns empty collection
        final ListContainerInstancesRequest listRequest = new ListContainerInstancesRequest();
        listRequest.setCluster(CLUSTER);
        final ListContainerInstancesResult listResult = new ListContainerInstancesResult();
        listResult.setContainerInstanceArns(Collections.emptyList());
        when(ecsClient.listContainerInstances(listRequest)).thenReturn(listResult);
        // describe empty list of instances throws an API error
        final DescribeContainerInstancesRequest describeRequest = new DescribeContainerInstancesRequest();
        describeRequest.setCluster(CLUSTER);
        describeRequest.setContainerInstances(Collections.emptyList());
        final DescribeContainerInstancesResult describeResult = new DescribeContainerInstancesResult();
        describeResult.setContainerInstances(Collections.emptyList());
        when(ecsClient.describeContainerInstances(describeRequest)).thenThrow(new InvalidParameterException(CONTAINER_INSTANCE_EMPTY_ERROR));

        assertThatExceptionOfType(InvalidParameterException.class)
                .isThrownBy(() -> new EcsFacade(ecsClient).containerInstance(CLUSTER, INSTANCE_ID))
                .withMessageStartingWith(CONTAINER_INSTANCE_EMPTY_ERROR);
    }

    @Test
    void containerInstanceStatus_whenInstanceCannotBeFoundBecauseInstanceDoesNotExistInTheCluster() throws Exception {
        // List instances in cluster returns collection that does not contain the instance we are looking for
        final Set<String> instanceArns = Collections.singleton("someArn");
        final ListContainerInstancesRequest listRequest = new ListContainerInstancesRequest();
        listRequest.setCluster(CLUSTER);
        final ListContainerInstancesResult listResult = new ListContainerInstancesResult();
        listResult.setContainerInstanceArns(instanceArns);
        when(ecsClient.listContainerInstances(listRequest)).thenReturn(listResult);
        // describe instances will not return the instance we are looking for, but does return some instances
        final ContainerInstance containerInstance = new ContainerInstance();
        containerInstance.setEc2InstanceId("someEc2InstanceId");
        final DescribeContainerInstancesRequest describeRequest = new DescribeContainerInstancesRequest();
        describeRequest.setCluster(CLUSTER);
        describeRequest.setContainerInstances(instanceArns);
        final DescribeContainerInstancesResult describeResult = new DescribeContainerInstancesResult();
        describeResult.setContainerInstances(Collections.singletonList(containerInstance));
        when(ecsClient.describeContainerInstances(describeRequest)).thenReturn(describeResult);

        assertThatExceptionOfType(InstanceNotFoundException.class)
                .isThrownBy(() -> new EcsFacade(ecsClient).containerInstance(CLUSTER, INSTANCE_ID).getStatus())
                .withMessageStartingWith("Could not find ECS container instance");
    }

    @Test
    void containerInstanceStatus_whenInstanceIsFound() throws Exception {
        final ContainerInstance someContainerInstance = new ContainerInstance();
        someContainerInstance.setEc2InstanceId(SOME_INSTANCE_ID);
        someContainerInstance.setStatus("ACTIVE");
        someContainerInstance.setContainerInstanceArn(SOME_ARN);
        final ContainerInstance otherContainerInstance = new ContainerInstance();
        otherContainerInstance.setEc2InstanceId(OTHER_INSTANCE_ID);
        otherContainerInstance.setStatus("ACTIVE");
        otherContainerInstance.setContainerInstanceArn(OTHER_ARN);
        final ContainerInstance expectedContainerInstance = new ContainerInstance();
        expectedContainerInstance.setEc2InstanceId(INSTANCE_ID);
        expectedContainerInstance.setStatus("DRAINING");
        expectedContainerInstance.setContainerInstanceArn(INSTANCE_ARN);
        // List instances in cluster returns collection that does contains the instance we are looking for
        final ListContainerInstancesRequest listRequest = new ListContainerInstancesRequest();
        listRequest.setCluster(CLUSTER);
        final ListContainerInstancesResult listResult = new ListContainerInstancesResult();
        final List<String> containerInstanceArns = Arrays.asList(SOME_ARN, INSTANCE_ARN, OTHER_ARN);
        listResult.setContainerInstanceArns(containerInstanceArns);
        when(ecsClient.listContainerInstances(listRequest)).thenReturn(listResult);
        // describe instances will return the instance we are looking for and a few others
        final DescribeContainerInstancesRequest describeRequest = new DescribeContainerInstancesRequest();
        describeRequest.setCluster(CLUSTER);
        describeRequest.setContainerInstances(containerInstanceArns);
        final DescribeContainerInstancesResult describeResult = new DescribeContainerInstancesResult();
        describeResult.setContainerInstances(Arrays.asList(someContainerInstance, expectedContainerInstance, otherContainerInstance));
        when(ecsClient.describeContainerInstances(describeRequest)).thenReturn(describeResult);

        final ContainerInstanceStatus instanceStatus = ContainerInstanceStatus.fromValue(new EcsFacade(ecsClient).containerInstance(CLUSTER, INSTANCE_ID).getStatus());

        assertThat(instanceStatus).isNotNull()
                                  .isEqualTo(ContainerInstanceStatus.DRAINING);
    }
}

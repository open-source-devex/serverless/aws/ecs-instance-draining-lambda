package com.gitlab.osdevex.ecs.instancedraining;

import static com.gitlab.osdevex.ecs.instancedraining.TestConstants.LIFECYCLE_HOOK_NAME;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import java.util.Collections;

import com.amazonaws.services.lambda.runtime.events.SNSEvent;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class SnsEventValidatorTest {

    private ObjectMapper mapper;
    private SnsEventValidator snsEventValidator;

    @BeforeEach
    void setUp() {
        mapper = new ObjectMapper();
        mapper.findAndRegisterModules();
        mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);

        snsEventValidator = new SnsEventValidator(mapper);
    }

    @Test
    void validateEvent_nullRecords() throws Exception {
        assertThatExceptionOfType(NullPointerException.class)
                .isThrownBy(() -> snsEventValidator.validateEvent(new SNSEvent(), LIFECYCLE_HOOK_NAME));
    }

    @Test
    void validateEvent_emptyRecords() throws Exception {
        final SNSEvent event = new SNSEvent();
        event.setRecords(Collections.emptyList());

        assertThatExceptionOfType(IllegalArgumentException.class)
                .isThrownBy(() -> snsEventValidator.validateEvent(event, LIFECYCLE_HOOK_NAME))
                .withMessage(SnsEventValidator.EVENT_MUST_HAVE_RECORDS)
                .withNoCause();
    }

    @Test
    void validateEvent_nullMessage() throws Exception {
        final SNSEvent.SNSRecord record = new SNSEvent.SNSRecord();
        record.setSns(new SNSEvent.SNS());
        final SNSEvent event = new SNSEvent();
        event.setRecords(Collections.singletonList(record));

        assertThatExceptionOfType(IllegalArgumentException.class)
                .isThrownBy(() -> snsEventValidator.validateEvent(event, LIFECYCLE_HOOK_NAME))
                .withMessage(SnsEventValidator.FIRST_EVENT_RECORD_MUST_HAVE_A_MESSAGE)
                .withNoCause();
    }

    @Test
    void validateEvent_emptyMessage() throws Exception {
        final SNSEvent.SNS sns = new SNSEvent.SNS();
        sns.setMessage("");
        final SNSEvent.SNSRecord record = new SNSEvent.SNSRecord();
        record.setSns(sns);
        final SNSEvent event = new SNSEvent();
        event.setRecords(Collections.singletonList(record));

        assertThatExceptionOfType(IllegalArgumentException.class)
                .isThrownBy(() -> snsEventValidator.validateEvent(event, LIFECYCLE_HOOK_NAME))
                .withMessage(SnsEventValidator.FIRST_EVENT_RECORD_MUST_HAVE_A_MESSAGE)
                .withNoCause();
    }

    @Test
    void validateEvent_notLifecycleHookMessage() throws Exception {
        final SNSEvent.SNS sns = new SNSEvent.SNS();
        sns.setMessage("Some message");
        final SNSEvent.SNSRecord record = new SNSEvent.SNSRecord();
        record.setSns(sns);
        final SNSEvent event = new SNSEvent();
        event.setRecords(Collections.singletonList(record));

        assertThatExceptionOfType(IllegalArgumentException.class)
                .isThrownBy(() -> snsEventValidator.validateEvent(event, LIFECYCLE_HOOK_NAME))
                .withMessage(SnsEventValidator.FIRST_EVENT_RECORD_MESSAGE_WAS_NOT_OF_CORRECT_TYPE);
    }

    @Test
    void validateEvent_noInstanceId() throws Exception {
        final SNSEvent.SNS sns = new SNSEvent.SNS();
        sns.setMessage(new SnsEventLifecycleHookMessage().toString());
        final SNSEvent.SNSRecord record = new SNSEvent.SNSRecord();
        record.setSns(sns);
        final SNSEvent event = new SNSEvent();
        event.setRecords(Collections.singletonList(record));

        assertThatExceptionOfType(NullPointerException.class)
                .isThrownBy(() -> snsEventValidator.validateEvent(event, LIFECYCLE_HOOK_NAME));
    }
}

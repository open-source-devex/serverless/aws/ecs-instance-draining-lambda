[![pipeline status](https://gitlab.com/open-source-devex/aws-ecs-instance-draining-lambda/badges/master/pipeline.svg)](https://gitlab.com/open-source-devex/aws-ecs-instance-draining-lambda/commits/master)
[![coverage report](https://gitlab.com/open-source-devex/aws-ecs-instance-draining-lambda/badges/master/coverage.svg)](https://gitlab.com/open-source-devex/aws-ecs-instance-draining-lambda/commits/master)

# ECS Instance Draining

This projects builds a jar that can be deployed to AWS Lambda to trigger a AWS ECS instance to go into the DRAINING state.
See [AWS ECS Developer Guide entry on this](http://docs.aws.amazon.com/AmazonECS/latest/developerguide/container-instance-draining.html) for the full documentation of the process.

The code and documentation in this repository started off as an (Java) implementation of the work posted in a blog post about automating ECS instance draining in response to changes in auto scaling groups.
Credits for designing and implementing this solution go to the authors of the work reported in the blog post.
We have simply ported the AWS Lambda function to Java, leveraging Spring Cloud.

## Requirements

AWS Lambda requires a role to execute with.
For the purpose of draining an ECS instance, that role requires permissions on a few AWS APIs.
Those permissions can be captured in the combination of a predefined AWS policy document ('arn:aws:iam::aws:policy/service-role/AutoScalingNotificationAccessRole') and a custom policy document that must be attached to the role.

Custom policy document:
```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "autoscaling:CompleteLifecycleAction",
                "logs:CreateLogGroup",
                "logs:CreateLogStream",
                "logs:PutLogEvents",
                "ec2:DescribeInstances",
                "ec2:DescribeInstanceAttribute",
                "ec2:DescribeInstanceStatus",
                "ec2:DescribeHosts",
                "ecs:ListContainerInstances",
                "ecs:SubmitContainerStateChange",
                "ecs:SubmitTaskStateChange",
                "ecs:DescribeContainerInstances",
                "ecs:UpdateContainerInstancesState",
                "ecs:ListTasks",
                "ecs:DescribeTasks",
                "sns:Publish",
                "sns:ListSubscriptions"
            ],
            "Effect": "Allow",
            "Resource": [
                "*"
            ]
        }
    ]
}
```

AWS Lambda needs to be able to assume that role, therefore the role must allow that:
```json
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow"
    }
  ]
}
```


## Deploy

### New function
```bash
./gradlew shadowJar
aws lambda create-function \
  --publish --runtime java8 --region eu-central-1 \
  --timeout 30 --memory-size 512 \
  --function-name Printer \
  --description "Printer function" \
  --zip-file fileb://build/libs/ecs-instance-draining-1.0-SNAPSHOT-aws.jar  \
  --role arn:aws:iam::415033069301:role/lambda-ecs-instance-draining-role \
  --handler org.springframework.cloud.function.adapter.aws.SpringBootStreamHandler
```

### Update function
```bash
./gradlew shadowJar
aws lambda update-function-code \
  --publish --region eu-central-1 \
  --function-name Printer \
  --zip-file fileb://build/libs/ecs-instance-draining-1.0-SNAPSHOT-aws.jar 
```
